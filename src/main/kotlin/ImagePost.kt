import kotlinx.browser.document
import kotlinx.html.*
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLInputElement


class ImagePost {

    private val usernameInput by lazy { document.getElementById("usernameTextInput") as HTMLInputElement }
    private val captionInput by lazy { document.getElementById("captionTextInput") as HTMLInputElement }
    private val imageTextInput by lazy { document.getElementById("imageTextInput") as HTMLInputElement }

    fun TagConsumer<HTMLElement>.render(callback: (String, String, String) -> Unit) {
        div(classes = "image__post") {
            input(InputType.text) {
                placeholder = "Enter a user name"
                id = "usernameTextInput"

            }
            input(InputType.text) {
                placeholder = "Enter a caption"
                id = "captionTextInput"

            }
            input(InputType.text) {
                placeholder = "Enter an image URL"
                id = "imageTextInput"
            }

            button {
                +"Post"
                onClickFunction = {
                    callback(usernameInput.value, captionInput.value, imageTextInput.value)
                    usernameInput.value = ""
                    captionInput.value = ""
                    imageTextInput.value = ""
                }
            }
        }
    }
}