import kotlinx.html.*
import org.w3c.dom.HTMLElement

class Post(private val username: String,
           private val caption: String,
           private val imageUrl: String) {

    fun TagConsumer<HTMLElement>.render() {
        div(classes = "post") {
            div(classes = "post__header") {

                img(classes = "post__avatar", src = "http://placeimg.com/80/80/animals", alt = "CatCitCut")

                h3 {
                    +username
                }
            }

            img(classes = "post__image", src = imageUrl)

            h4(classes = "post__text") {
                strong {
                    +username
                }
                +" $caption"
            }
        }
    }
}