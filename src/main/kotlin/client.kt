import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.html.div
import kotlinx.html.dom.append
import kotlinx.html.h1
import kotlinx.html.id
import kotlinx.html.img
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.Node

fun main() {
    window.onload = { document.body?.buildPage() }
}

fun Node.buildPage() {
    append {
        div("app") {
            div("app__header") {

                img(
                    classes = "app__headerImage",
                    alt = "",
                    src = "https://www.instagram.com/static/images/web/mobile_nav_type_logo-2x.png/1b47f9d0e595.png"
                )
            }
            h1 {
                +"Hello from JS"
            }

            val posts = listOf(
                Post(
                    username = "Username",
                    caption = "OWOWOW kdjflsjdklf lsdkfjlksd  lkjdfscaption",
                    imageUrl = "https://www.freecodecamp.org/news/content/images/2020/02/Ekran-Resmi-2019-11-18-18.08.13.png"
                ),
                Post(
                    username = "Instagram",
                    caption = "Blablba wowoww",
                    imageUrl = "https://logodix.com/logo/585776.png"
                )
            )


            div(classes = "app__posts") {
                id = "postContainer"
                posts.forEach { it.run { render() } }
            }

            with(ImagePost()) {
                render { username, caption, imageUrl ->
                    val postContainer = document.getElementById("postContainer") as HTMLDivElement
                    postContainer.append {
                        Post(username, caption, imageUrl).run { render() }
                    }

                }
            }
        }
    }
}